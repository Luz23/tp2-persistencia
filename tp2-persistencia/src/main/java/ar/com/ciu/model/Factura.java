package ar.com.ciu.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;



	@Entity(name="Factura")
	@Table(name="factura")
	public class Factura {

			// atributos
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
		@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
		private Long id;
		

		@ManyToOne
		@JoinColumn(name = "cliente_id", foreignKey=@ForeignKey(name="cliente_factura_cliente"))
		private Cliente cliente;
		
		@Column
		private LocalDateTime fecha;
		
		@Column
		private Integer numero;
		
		@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true)
		@JoinColumn(name = "detalle_id") 
		private List<Detalle> detalles;
		
		
			// constructores
		
		public Factura(){
			super();
		}
		
		
		public Factura(Cliente cliente, LocalDateTime fecha, Integer num) {
			super();
			
			this.cliente = cliente;
			//cliente.addFactura(this);
			this.fecha = fecha;
			this.numero = num;
			}


		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Factura other = (Factura) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}


		public Long getId() {
			return id;
		}


		public void setId(Long id) {
			this.id = id;
		}


		public Cliente getCliente() {
			return cliente;
		}


		public void setCliente(Cliente cliente) {
			this.cliente = cliente;
		}


		public LocalDateTime getFecha() {
			return fecha;
		}


		public void setFecha(LocalDateTime fecha) {
			this.fecha = fecha;
		}


		public Integer getNumero() {
			return numero;
		}


		public void setNumero(Integer numero) {
			this.numero = numero;
		}


		public List<Detalle> getDetalles() {
			return detalles;
		}


		public void setDetalles(List<Detalle> detalles) {
			this.detalles = detalles;
		}
		
		
		public void addDetalle(Detalle detalle){
			this.detalles.add(detalle);
		}

}
