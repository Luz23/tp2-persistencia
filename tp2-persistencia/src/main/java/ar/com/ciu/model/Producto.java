package ar.com.ciu.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;




@Entity(name="Producto")
@Table(name="producto")
public class Producto {
	
	
	// atributos
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	private Long id;
	
	
	@Column
	private String codigo;
	
	@Column
	private String descripcion;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="precio_id", foreignKey=@ForeignKey(name="precio_producto_precio"))
	private Precio precio;
	
	
	
	@ManyToMany(mappedBy="productos")
	private List<Proveedor> proveedores = new ArrayList<Proveedor>();
	
	// constructores
	
	public Producto(){
		super();
	}
	
	
	public Producto(String codigo, String descripcion, Precio precio) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.precio = precio;
		
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producto other = (Producto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Precio getPrecio() {
		return precio;
	}


	public void setPrecio(Precio precio) {
		this.precio = precio;
	}


	public List<Proveedor> getProveedores() {
		return proveedores;
	}


	public void setProveedores(List<Proveedor> proveedores) {
		this.proveedores = proveedores;
	}
	
	public void addProveedor(Proveedor prov){
		proveedores.add(prov);
	}
	

}
