package ar.com.ciu.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;



@Entity(name="Detalle")
@Table(name="detalle")
public class Detalle {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	private Long id;
	

	@ManyToOne
	@JoinColumn(name = "factura_id", foreignKey=@ForeignKey(name="factura_detalle_factura"))
	private Factura factura;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "producto_id")
	private Producto producto;
	
	@Column
	private Integer cantidad;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "precio_id")
	private Precio precioVenta;

	
	public Detalle() {
		super();
	}

	
	public Detalle (Factura factura, Integer cantidad, Producto prod) {
		super();
		this.factura = factura;
		this.cantidad = cantidad;
		this.producto = prod;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Detalle other = (Detalle) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Factura getFactura() {
		return factura;
	}


	public void setFactura(Factura factura) {
		this.factura = factura;
	}


	public Integer getCantidad() {
		return cantidad;
	}


	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
	
	
	
	


}
