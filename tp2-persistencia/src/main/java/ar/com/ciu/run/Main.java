package ar.com.ciu.run;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.hibernate.Session;

import ar.com.ciu.hibernate.HibernateUtil;
import ar.com.ciu.model.Cliente;
import ar.com.ciu.model.Detalle;
import ar.com.ciu.model.Factura;
import ar.com.ciu.model.Producto;
import ar.com.ciu.model.Proveedor;
import ar.com.ciu.model.Precio;

public class Main {
	

	public static void main(String[] args) {
			//Punto 5
			Cliente cliente1 = new Cliente("11", "perez", "juan", "1");
			Cliente cliente2 = new Cliente("22", "sosa", "julio", "2");
			Cliente cliente3 = new Cliente("33", "paso", "pepe", "3");
			
			
			
			
			Factura factura1 = new Factura(cliente1, LocalDateTime.now(), 1);
			Factura factura2 = new Factura(cliente1, LocalDateTime.now(), 2);
			Factura factura3 = new Factura(cliente2, LocalDateTime.now(), 3);
			
		
			
			//Punto 6	
		
			Proveedor pepe = new Proveedor("pepe", "11");
			Proveedor juan = new Proveedor("juan", "22");
			Proveedor julio = new Proveedor("julio", "33");
			
		    
			Producto prod1 = new Producto("11aa", "azucar", null );
			Producto prod2 = new Producto("22aa", "papa", null );
			Producto prod3 = new Producto("33aa", "harina",null);
			

			prod1.setPrecio( new Precio(new BigDecimal(61.80), LocalDateTime.now(), prod1) );
			
			prod2.setPrecio( new Precio(new BigDecimal(61.80),  LocalDateTime.now(), prod2) );
			
			
			Detalle detalle = new  Detalle(factura1, 1, prod1 );
			Detalle detalle2 = new  Detalle(factura1, 2 , prod2);

			 
			Session session = HibernateUtil.getSessionFactory().openSession();
			try {
				session.beginTransaction();
				//Punto 5
				session.persist(cliente1);
				session.persist(cliente2);
				session.persist(cliente3);
				
				session.persist(factura1);
				session.persist(factura2);
				session.persist(factura3);
				
				
				session.persist(detalle);
				session.persist(detalle2);
				
				//Punto 6	

				session.persist(pepe);
				session.persist(juan);
				session.persist(julio);
				
				
				//punto 7
				prod1.addProveedor(juan);
				prod1.addProveedor(pepe);
				session.save(prod1);
				
				prod2.addProveedor(pepe);
				session.persist(prod2);
				
				prod3.addProveedor(juan);
				session.persist(prod3);
				
				session.getTransaction().commit();
				//punto 8
				Precio nuevoPrecio = new Precio(new BigDecimal(1300), LocalDateTime.now(), prod2);
				prod2.setPrecio(nuevoPrecio);
				session.beginTransaction();
				session.persist(prod2);
				session.getTransaction().commit();
				
			} catch (Exception e) {
				e.printStackTrace();
				session.getTransaction().rollback();
			} finally {
				session.close();
			}

			HibernateUtil.getSessionFactory().close();
		}

}
